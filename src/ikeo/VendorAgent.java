/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ikeo;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

/**
 *
 * @author T430
 */
public class VendorAgent extends Agent {
    private VendorAgentGUI vendorAgentGui;
    private AID[] sellerAgents;

    // Agent initialisation
    protected void setup() {
        // Initialise GUI
        vendorAgentGui = new VendorAgentGUI();
        vendorAgentGui.showGui();
        
        vendorAgentGui.updateConsole("VendorAgent " + getAID().getName() + " initialised.");
        
        // Find SellerAgents currently running every 5 seconds
        addBehaviour(new TickerBehaviour(this, 5000) {
            protected void onTick() {
                vendorAgentGui.updateConsole("Searching for SellerAgents");
                
                // Update the list of seller agents
                DFAgentDescription template = new DFAgentDescription();
                ServiceDescription sd = new ServiceDescription();
                sd.setType("book-selling");
                template.addServices(sd);
                
                try {
                    DFAgentDescription[] results = DFService.search(myAgent, template); 
                    
                    if (results.length > 0) {
                        vendorAgentGui.updateConsole("Found the following SellerAgents:");
                        sellerAgents = new AID[results.length];
                        for (int i = 0; i < results.length; ++i) {
                            sellerAgents[i] = results[i].getName();
                            vendorAgentGui.updateConsole(sellerAgents[i].getName());
                        }
                    }
                    
                    else {
                        vendorAgentGui.updateConsole("No SellerAgents found!");
                    }                    
                }
                catch (FIPAException fe) {
                    fe.printStackTrace();
                }
            }
        });
        
    }
    
    // Run this function when doDelete() is called
    protected void takeDown() {
        // Printout a dismissal message
        System.out.println("Vendor Agent " + getAID().getName() + " terminating.");
    }    
}
